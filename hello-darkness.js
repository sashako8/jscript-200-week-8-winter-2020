let color = 255;

let countdownInterval = setInterval(function() {
    if (color > 0) {
        document.querySelector('body').style.backgroundColor = `rgb(${color}, ${color}, ${color})`;
        color--;
    } else {
        clearInterval(countdownInterval);
    }
}, 500
)