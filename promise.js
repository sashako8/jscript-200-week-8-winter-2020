let myPromise = new Promise(function(resolve, reject) {
  setTimeout(function() {
    const random = Math.random();
    if (random > 0.5) {
      resolve(random);
    } else {
      reject();
    }
  }, 1000);
});

myPromise.then(number => {
  console.log('success');
  console.log('complete');
})
 .catch(e => {
   console.log('fail');
   console.log('complete');
 })

