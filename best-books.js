const formEl = document.getElementById('best-books-form');
const yearEl = document.getElementById('year');
const monthEl = document.getElementById('month');
const dateEl = document.getElementById('date');

formEl.addEventListener('submit', function(e) {
  e.preventDefault();
 document.getElementById('book').innerText = '';

  const year = yearEl.value;
  const month = monthEl.value;
  const date = dateEl.value;

  // Fetch bestselling books for date and add top 5 to page
  const url = `https://api.nytimes.com/svc/books/v3/lists/${year}-${month}-${date}/hardcover-fiction.json?api-key=ZPvnAwkWSiPA01MolWzytIifRi5P9csW`;
  fetch(url)
    .then(function(data) {
      return data.json();
    })
    .then(function(responseJson) {
      console.log(responseJson);
      for (var i = 0; i < 5; i++) {
        let book = responseJson.results.books[i];
        const title = book.title;
        const author = book.author;
        const description = book.description;
        //const image = `https://s1.nyt.com/du/books/images/${book.primary_isbn13}.jpg`;

        document.getElementById('book').innerText += title + '\n' + author + '\n' + description + '\n' + '\n';
      }
    })
});
