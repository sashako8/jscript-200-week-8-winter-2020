let color = 0;

let requestAnimationFrame = setInterval(function() {
    if (color < 256) {
        document.querySelector('body').style.backgroundColor = `rgb(${color}, ${color}, ${color})`;
        color++;
    } else {
        clearInterval(requestAnimationFrame);
    }
}, 20
)