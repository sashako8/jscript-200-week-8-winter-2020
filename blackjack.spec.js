describe('Blackjack', () => {
    describe('calcPoints', () => {
            const ten = {
                val: 10,
                displayVal:'10'
            };
            const six = {
                val: 6,
                displayVal:'6'
            };            
            const ace = {
                val: 11,
                displayVal: 'Ace'
            };

            it('Should count the total points in my hand', () => {
                const points = calcPoints([ten, six]);
                
                const expected = {
                    total: 16,
                    isSoft:false
                }
            expect(points).toEqual(expected);
            })
            it('Handles ace correctly', () => {
                const points = calcPoints([ten, six, ace]);
                
                const expected = {
                    total: 17,
                    isSoft:false
                }
            expect(points).toEqual(expected);
            })
            it('Handles ace correctly', () => {
                const points = calcPoints([six, ace]);
                
                const expected = {
                    total: 17,
                    isSoft:true
                }
            expect(points).toEqual(expected);
            })
            it('Handles ace correctly', () => {
                const points = calcPoints([ace, ace]);
                
                const expected = {
                    total: 12,
                    isSoft:true
                }
            expect(points).toEqual(expected);
            })
            it('Handles ace correctly', () => {
                const points = calcPoints([six, ace, ace]);
                
                const expected = {
                    total: 18,
                    isSoft:true
                }
            expect(points).toEqual(expected);
            })
    })

    describe('dealerShouldDraw', () => {
        const ten = {
            val: 10,
            displayVal:'10'
        };
        const nine = {
            val: 9,
            displayVal:'9'
        };
        const six = {
            val: 6,
            displayVal:'6'
        };            
        const ace = {
            val: 11,
            displayVal: 'Ace'
        };
        const two = {
            val: 2,
            displayVal: '2'
        };
        const four = {
            val: 4,
            displayVal: '4'
        };
        const five = {
            val: 5,
            displayVal: '5'
        };
        
        it('Handles whether dealer should draw correctly', () => {
            const hand = dealerShouldDraw([ten, nine]);
            const expected = false;
            expect(hand).toBe(expected);
        })
        it('Handles whether dealer should draw correctly', () => {
            const hand = dealerShouldDraw([ace, six]);
            const expected = true;
            expect(hand).toBe(expected);
        })
        it('Handles whether dealer should draw correctly', () => {
            const hand = dealerShouldDraw([ten, ace, six]);
            const expected = false;
            expect(hand).toBe(expected);
        })
        it('Handles whether dealer should draw correctly', () => {
            const hand = dealerShouldDraw([two, four, two, five]);
            const expected = true;
            expect(hand).toBe(expected);
        })
    })
})