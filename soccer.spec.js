describe('soccer functions', () => {
    describe('getTotalPoints', () => {
        it('Should give me 7 points for "wwdl"', () => {
            const points = getTotalPoints('wwdl');

            expect(points).toBe(7);
        })
    })
})

describe('orderTeams', () => {
    it('Should output correct team "name: points"', () => {
        const team = {
            name: 'Sounders',
            results: 'wdlw'
        }
        const output = orderTeams(team);
        expect(output).toBe('Sounders: 7');
    })
})